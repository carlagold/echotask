//
//  DetailsViewController.swift
//  EchoTask
//
//  Created by Carla on 2017/07/05.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import UIKit
import CoreLocation

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nearestAuthorLabel1: UILabel!
    @IBOutlet weak var nearestAuthorLabel2: UILabel!
    
    let networkManager = NetworkManager.sharedInstance
    var userId : Int? = nil
    var user : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeView()
        self.title = "Details"
        self.populateUserDetails()
    }
    
    func populateUserDetails() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.networkManager.getUsers(completion: { (users, errorMessage) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if var users = users {
                //find user
                if let userId = self.userId {
                    if let i = users.index(where: { $0.userId == userId }) {
                        self.user = users[i]
                        self.configureView()
                    }
                    self.getNearestAuthors(users: users)
                }
            }
            if !errorMessage.isEmpty { print("Error: " + errorMessage) }
        })
        
    }
    
    func getNearestAuthors(users: [User]) {
        
        if (users.count == 1) { //users[0] will be the current user's details.
            return
        }
        
        var user1 : User?
        var distanceFromUser1 = Double.infinity
        
        var user2 : User?
        var distanceFromUser2 = Double.infinity
        
        for user in users {
            if (self.userId == user.userId) { continue }
            
            let distance = self.user!.location.distance(from: user.location)
            
            if (distanceFromUser1 >= distanceFromUser2 && distance < distanceFromUser1) {
                distanceFromUser1 = distance
                user1 = user
            } else if (distanceFromUser2 >= distanceFromUser1 && distance < distanceFromUser2) {
                distanceFromUser2 = distance
                user2 = user
            }
        }
        
        if let user1 = user1 {
            self.nearestAuthorLabel1.text = "\(user1.name) \((distanceFromUser1 / 10).rounded() / 100 )km away"
        }
        
        if let user2 = user2 {
            self.nearestAuthorLabel2.text = "\(user2.name) \((distanceFromUser2 / 10).rounded() / 100 )km away"
        }
    }
    
    
    func initializeView() {
        self.nameLabel.text = ""
        self.emailLabel.text = ""
        self.nearestAuthorLabel1.text = ""
        self.nearestAuthorLabel2.text = ""
    }
    
    func configureView() {
        self.nameLabel.text = user?.name
        self.emailLabel.text = user?.email
    }
    
    
}
