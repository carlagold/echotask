//
//  User.swift
//  EchoTask
//
//  Created by Carla on 2017/07/05.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import Foundation
import CoreLocation

class User {
    let userId: Int
    let name: String
    let email: String
    let location: CLLocation
    
    init(name: String, email: String, userId: Int, location: CLLocation) {
        self.name = name
        self.email = email
        self.userId = userId
        self.location = location
    }
    
}
