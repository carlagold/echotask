//
//  NetworkManager.swift
//  EchoTask
//
//  Created by Carla on 2017/07/05.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import Foundation
import CoreLocation

class NetworkManager {
    static let sharedInstance = NetworkManager()

    typealias JSONDictionary = [String: Any]
    
    var posts: [Post] = []
    var users: [User] = []
    
    var errorMessage = ""
    
    var defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    init() {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30.0
        defaultSession = URLSession(configuration: sessionConfig)
    }
    
    //MARK: PUBLIC FUNCTIONS
    
    func getUsers(completion: @escaping ([User]?, String) -> ()) {
        if (self.users.count > 0) {
            completion(self.users, self.errorMessage)
            return
        }
        
        let url = URL(string: "https://jsonplaceholder.typicode.com/users/")
        
        self.makeRequest(url: url!) { (data, errorMessage) in
            self.updateUsers(data)
            DispatchQueue.main.async {
                completion(self.users, self.errorMessage)
            }
        }
    }
    
    func getPosts(completion: @escaping ([Post]?, String) -> ()) {
        
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts/")
        
        self.users = []; //if we are redownloading the posts, we will need to get latest users later on. Can't reuse previously downloaded.
        self.makeRequest(url: url!) { (data, errorMessage) in
            self.updatePosts(data)
            self.getComments(completion: { (commentsData, errorMessage) in
                DispatchQueue.main.async {
                    completion(self.posts, self.errorMessage)
                }
            })
        }
    }
    
    func getComments(completion: @escaping ([Post]?, String) -> ()) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/comments/")
        
        self.makeRequest(url: url!) { (data, errorMessage) in
            self.updatePostsWithComments(data)
            DispatchQueue.main.async {
                completion(self.posts, self.errorMessage)
            }
        }
    }

    //MARK: PRIVATE FUNCTIONS
    
    private func makeRequest(url: URL, completion: @escaping (Data?, String) -> ()) {
        dataTask?.cancel() //cancels existing data task if a task already exists so we can reuse for new query
        
        dataTask = defaultSession.dataTask(with: url) { data, response, error in
            
            defer { self.dataTask = nil } //clean up. sets data task to nil after return
            
            if let error = error {
                
                self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
                
                completion(nil, self.errorMessage)

            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                
                completion(data, self.errorMessage)
                
            } else {
                
                completion(nil, self.errorMessage)
                
            }
        }
        
        dataTask?.resume() //starts the task
    }
    
    
    private func updatePosts(_ data: Data?) {
        
        guard let response = parseDataToJSON(data) as [JSONDictionary]! else {
            return
        }
        
        posts.removeAll()
        
        for dict in response {
            if let post = parsePost(dict) {
                self.posts.append(post)
            }
        }
        
    }
    
    private func updateUsers(_ data: Data?) {
        
        guard let response = parseDataToJSON(data) as [JSONDictionary]! else {
            return
        }
        
        users.removeAll()
        
        for dict in response {
            if let user = parseUser(dict) {
                self.users.append(user)
            }
        }
    }
    
    private func updatePostsWithComments(_ data: Data?) {
        
        guard let response = parseDataToJSON(data) as [JSONDictionary]! else {
            return
        }
        
        
        //create hash table of postId and commentCount
        var commentsOnPost : [Int : Int] = [:]
        for dict in response {
            if let postId = dict["postId"] as? Int {
                if (commentsOnPost[postId] == nil) {
                    commentsOnPost[postId] = 1
                } else {
                    commentsOnPost[postId]! += 1
                }
            }
        }
        
        //use hashtable to update each post with the comment count
        for post in self.posts {
            post.numberOfComments = commentsOnPost[post.index]
        }
    }
    
    private func parseDataToJSON(_ data: Data?) -> [JSONDictionary]? {
        guard let data = data else {
            return nil
        }
        
        var response: Any?
        
        do {
            response = try JSONSerialization.jsonObject(with: data, options: [])
        } catch let parseError as NSError {
            errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
        }
        
        if let response = response as? [JSONDictionary] {
            return response
        } else {
            errorMessage += "Problem parsing dictionary\n"
        }
        
        return nil
    }
    
    //MARK: Parse

    private func parsePost(_ dict: JSONDictionary?) -> Post? {
        if let dict = dict,
            let title = dict["title"] as? String,
            let body = dict["body"] as? String,
            let userId = dict["userId"] as? Int,
            let index = dict["id"] as? Int {
            let post = Post(title: title, body: body, userId: userId, index: index)
            return post
        }
        
        return nil
    }
    
    private func parseUser(_ dict: JSONDictionary?) -> User? {
        
        if let dict = dict,
            let name = dict["name"] as? String,
            let email = dict["email"] as? String,
            let address = dict["address"] as? JSONDictionary,
            let userId = dict["id"] as? Int,
            let geo = address["geo"] as? JSONDictionary,
            let lat = geo["lat"] as? String,
            let lon = geo["lng"] as? String {
            let point = CLLocation(latitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(lon)!)
            let user = User(name: name, email: email, userId: userId, location: point)
            return user
        }
        
        return nil
    }
    
}




