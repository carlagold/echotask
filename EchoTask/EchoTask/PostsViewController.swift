//
//  ViewController.swift
//  EchoTask
//
//  Created by Carla on 2017/07/05.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import UIKit

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var postsTableView: UITableView!
    @IBOutlet weak var refreshButton: UIButton!
    
    var posts: [Post] = []
    let networkManager = NetworkManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Posts"
        self.postsTableView.dataSource = self
        self.postsTableView.delegate = self
        self.populateTableView()
    }
    
    
    func populateTableView() {
        self.refreshButton.isEnabled = false;
        self.refreshButton.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 214/255, blue: 163/255, alpha: 0.3);
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.networkManager.getPosts(completion: { (results, errorMessage) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if let results = results {
                self.posts = results
                self.postsTableView.reloadData()
                self.refreshButton.isEnabled = true;
                self.refreshButton.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 214/255, blue: 163/255, alpha: 1.0);

            }
            if !errorMessage.isEmpty { print("Error: " + errorMessage) }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetails" {
            let detailsVC: DetailsViewController = segue.destination as! DetailsViewController
            if let cell = sender as? PostTableViewCell {
                let indexPath = self.postsTableView.indexPath(for: cell)
                let index = indexPath?.row
                let userId = posts[index!].userId
                detailsVC.userId = userId
            }
        }
    }
    
    @IBAction func didTapRefresh(_ sender: AnyObject) {
        self.posts = []
        self.postsTableView.reloadData()
        self.populateTableView()
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PostTableViewCell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostTableViewCell
        let post = posts[indexPath.row]
        cell.populate(post)
        return cell
        
    }
    
    
}

