//
//  Post.swift
//  EchoTask
//
//  Created by Carla on 2017/07/05.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import Foundation

class Post {
    
    let title: String
    let body: String
    let userId: Int
    let index: Int
    var numberOfComments: Int?
    
    init(title: String, body: String, userId: Int, index: Int) {
        self.title = title
        self.body = body
        self.userId = userId
        self.index = index
    }
    
}
