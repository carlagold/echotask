//
//  PostTableViewCell.swift
//  EchoTask
//
//  Created by Carla on 2017/07/05.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import UIKit

class PostTableViewCell : UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    
    func populate(_ post: Post) {
        var body = post.body
        if (body.characters.count > 18) {
            body = body.substring(to: body.index(body.startIndex, offsetBy: 18))
            body = body + "..."
        }
        
        bodyLabel.text = body
        titleLabel.text = post.title
        commentCountLabel.text = ""
        if (post.numberOfComments != nil) {
            commentCountLabel.text = "\(post.numberOfComments!) comments"
        }
    }
}
